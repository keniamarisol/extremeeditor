/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gramaticaGXML;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Kenia
 */
public class Interprete {

    public List<String> erroresSemanticos = new ArrayList<>();
    List<String> erroresSintacticos = new ArrayList<>();
    List<String> importsGXML = new ArrayList<>();
    Pattern exprHexadecimal = Pattern.compile("[#][0-9a-fA-F]{6}+");
    Matcher matcher;
    String fscript = "";
    String ventana_actual = "";
    String contenedor_actual = "";
    boolean ventana_principal = false;

    public String iniciar(nodoArbol raiz) {

        String nom = raiz.produccion.trim();

        switch (nom) {
            case "GXML":
                recorrerGXML(raiz.hijos);
                break;
        }

        //System.out.println("*************Errores semanticos*************\n " + Arrays.toString(erroresSemanticos.toArray()));
        return fscript;

    }

    public void recorrerGXML(LinkedList<nodoArbol> hijos) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim().toUpperCase();
            switch (produccion) {
                case "LISTA_IMPORTS":
                    recorrerImport(hijo.hijos);
                    break;
                case "LISTA_VENTANAS":
                    recorrerVentana(hijo.hijos);
                    break;

            }
        }
    }

    public void recorrerImport(LinkedList<nodoArbol> hijos) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim();
            String ruta = "";
            switch (produccion) {
                case "IMPORTAR":
                    ruta = hijo.hijos.get(0).produccion;
                    if (ruta.contains(".gxml")) {
                        importsGXML.add(ruta);
                    } else {
                        fscript = "Importar(\"" + ruta + "\");\n\n";
                    }
                    break;
            }
        }
    }

    public void recorrerVentana(LinkedList<nodoArbol> hijos) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim();
            String ruta = "";
            switch (produccion) {
                case "VENTANA":
                    recorrerVentana(hijo.hijos);
                    break;
                case "LISTA_ELEMENTOS":
                    recorrerElementosVentana(hijo.hijos);
                    break;
                case "LISTA_CONTENEDORES":
                    recorrerContenedor(hijo.hijos);
                    break;
            }
        }
    }

    public void recorrerElementosVentana(LinkedList<nodoArbol> hijos) {
        String id = "";
        String tipo = "";
        String color = "";
        String accionInicial = "";
        String accionFinal = "";

        for (nodoArbol hijo : hijos) {
            String elemento = hijo.hijos.get(0).produccion;
            String valor = hijo.hijos.get(1).produccion;

            switch (elemento.toUpperCase()) {
                case "ID":
                    id = "ven_" + valor;
                    break;
                case "TIPO":
                    tipo = valor;
                    break;
                case "COLOR":
                    color = valor;
                    break;
                case "ACCIONINICIAL":
                    accionInicial = valor.substring(1, valor.length() - 1);
                    break;
                case "ACCIONFINAL":
                    accionFinal = valor.replace("{", "").replace("}", "");
                    break;
                default:
                    erroresSintacticos.add("Error Semántico, no se reconoce el elemento: " + elemento);
                    break;
            }

        }

        if (id.isEmpty() || tipo.isEmpty()) {

            erroresSemanticos.add("Error Semántico, no se encuentran todos los elementos obligatorios en la ventana: " + id);

        } else {

            ventana_actual = id;
            fscript += "var " + id + " = CrearVentana(";

            if (color.isEmpty()) {
                fscript += "\"#ffffff\", 300, 500,\""+id+"\");\n\n";
            } else if (exprHexadecimal.matcher(color).find()) {
                fscript += "\"" + color + "\", 300, 500,\""+id+"\"); \n\n";
            } else {
                erroresSemanticos.add("Error Semántico, el color no es válido en la ventana: " + id);
            }

           /* if (!accionInicial.isEmpty()) {
                fscript += id + ".AlCargar(" + accionInicial + ");\n\n";
            }

            if (!accionFinal.isEmpty()) {
                fscript += id + ".AlCerrar(" + accionFinal + ");\n\n";
            }

            if (tipo.equalsIgnoreCase("principal")) {
                ventana_principal = true;
                fscript += id + ".AlCargar();\n\n";
            } else if (tipo.equalsIgnoreCase("secundario")) {

            }*/

            //DUDA: qué se hace si la ventana principal trae accion inicial y final
        }

    }

    public void recorrerContenedor(LinkedList<nodoArbol> hijos) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim();
            switch (produccion) {
                case "CONTENEDOR":
                    recorrerContenedor(hijo.hijos);
                    break;
                case "LISTA_ELEMENTOS":
                    recorrerElementosContenedor(hijo.hijos);
                    break;
                case "CONTENIDO":
                    recorrerContenidoContenedor(hijo.hijos);
                    break;
            }
        }
    }

    public void recorrerElementosContenedor(LinkedList<nodoArbol> hijos) {
        String id = "";
        String x = "";
        String y = "";
        String alto = "";
        String ancho = "";
        String color = "";
        String borde = "";

        for (nodoArbol hijo : hijos) {
            String elemento = hijo.hijos.get(0).produccion;
            String valor = hijo.hijos.get(1).produccion;

            switch (elemento.toUpperCase()) {
                case "ID":
                    id = valor + "_" + ventana_actual;
                    break;
                case "X":
                    x = valor;
                    break;
                case "Y":
                    y = valor;
                    break;
                case "ALTO":
                    alto = valor;
                    break;
                case "ANCHO":
                    ancho = valor;
                    break;
                case "COLOR":
                    color = valor;
                    break;
                case "BORDE":
                    borde = color;
                    break;
                default:
                    erroresSemanticos.add("Error Semántico, no se reconoce el elemento: " + elemento);
                    break;
            }

        }

        if (id.isEmpty() || x.isEmpty() || y.isEmpty()) {
            erroresSemanticos.add("Error Semántico, no se encuentran todos los elementos obligatorios en contenedor: " + id);
        } else {

            contenedor_actual = id;
            fscript += "var " + id + " = " + ventana_actual + ".CrearContenedor(";

            if (alto.isEmpty()) {
                fscript += "500";
            } else if (StringUtils.isNumeric(alto)) {
                fscript += alto;
            } else {
                fscript += "500";
                erroresSemanticos.add("Error Semántico, el alto no es válido en el contenedor: " + id);
            }

            if (ancho.isEmpty()) {
                fscript += ",500";
            } else if (StringUtils.isNumeric(ancho)) {
                fscript += "," + ancho;
            } else {
                fscript += ",500";
                erroresSemanticos.add("Error Semántico, el ancho no es válido en el contenedor: " + id);
            }

            if (color.isEmpty()) {
                fscript += ",\"#ffffff\"";
            } else if (exprHexadecimal.matcher(color).find()) {
                fscript += ",\"" + color + "\"";
            } else {
                fscript += ",\"#ffffff\"";
                erroresSemanticos.add("Error Semántico, el color no es válido en el contenedor: " + id);
            }

            if (borde.isEmpty()) {
                fscript += ",falso";
            } else if (borde.equalsIgnoreCase("verdadero") || borde.equalsIgnoreCase("falso")) {
                fscript += "," + borde;
            } else {
                erroresSemanticos.add("Error Semántico, el valor para borde no es válido en el contenedor: " + id);
                fscript += ",falso";
            }

            if (StringUtils.isNumeric(x)) {
                fscript += "," + x;
            } else {
                fscript += ",0";
                erroresSemanticos.add("Error Semántico, la posición X no es válida en el contenedor: " + id);
            }

            if (StringUtils.isNumeric(y)) {
                fscript += "," + y;
            } else {
                fscript += ",0";
                erroresSemanticos.add("Error Semántico, la posición Y no es válida en el contenedor: " + id);
            }

            fscript += ");\n\n";
        }

    }

    public void recorrerContenidoContenedor(LinkedList<nodoArbol> hijos) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim();
            switch (produccion) {
                case "CONTROL":
                    recorrerControl(hijo.hijos);
                    break;
                case "ETIQUETA_TEXTO":
                    // recorrerElementosContenedor(hijo.hijos);
                    break;
                case "ETIQUETA_BOTON":
                    //recorrerContenedor(hijo.hijos);
                    break;
                case "ETIQUETA_ENVIAR":
                    //recorrerContenedor(hijo.hijos);
                    break;
                case "ETIQUETA_MULTIMEDIA":
                    //recorrerContenedor(hijo.hijos);
                    break;

            }
        }
    }
            

    public void recorrerControl(LinkedList<nodoArbol> hijos) {
        for (nodoArbol hijo : hijos) {
            String produccion = hijo.produccion.trim();
            switch (produccion) {

                case "LISTA_ELEMENTOS":
                  //  recorrerElementosControl(hijo.hijos);
                    break;
                case "CONTENIDO":
                 //   recorrerContenidoControl(hijo.hijos);
                    break;
            }
        }
    }


}
