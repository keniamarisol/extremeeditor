/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Kenia
 */
import java.util.LinkedList;


public class DeclaAsig implements Instruccion {

    
    protected TablaDeSimbolos tablaPadre;

    
    LinkedList<String> ids;
    
    protected final Instruccion valor;

    
    public DeclaAsig(LinkedList<String> a, Instruccion b) {
        ids = a;
        this.valor = b;
    }

    
    public void setTablaDeSimbolosPadre(TablaDeSimbolos ts) {
        this.tablaPadre = ts;
    }

    
    @Override
    public Object ejecutar(TablaDeSimbolos ts, Arbol ar) {

        if (tablaPadre != null) {// Si se definió una tabla padre, se obtendrá de ahí el valor a asignar.
            if (!ids.isEmpty()) {

                for (String id : ids) {
                    Simbolo aux = new Simbolo(id, Simbolo.Tipo.VAR);
                    aux.setParametro(false);
                    ts.add(aux);
                }

                ts.setValor(ids.getLast(), valor.ejecutar(tablaPadre, ar));
            }

        } else {
            for (String id : ids) {
                Simbolo aux = new Simbolo(id, Simbolo.Tipo.VAR);
                aux.setParametro(false);
                ts.add(aux);
            }
            ts.setValor(ids.getLast(), valor.ejecutar(ts, ar));
        }

        return null;
    }

}
