/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Kenia
 */

import java.util.LinkedList;


public class DeclaracionArreglo extends Declaracion implements Instruccion {

    
    private final LinkedList<Instruccion> elementos;

    
    public DeclaracionArreglo(String a, LinkedList<Instruccion> c) {
        super(a);
        elementos = c;
    }

    
    @Override
    public Object ejecutar(TablaDeSimbolos ts, Arbol ar) {
        LinkedList<Object> elements = new LinkedList<>();
        for (Instruccion el : elementos) {
            Object er = el.ejecutar(ts, ar);

            elements.add(er);
        }

        String flag = "";

        for (Object e : elements) {
            if (e instanceof Double) {
                if (flag.length() == 0) {
                    flag = "numerico";
                } else if (!flag.equalsIgnoreCase("numerico")) {
                    flag = "heterogeneo";
                    break;
                }
            } else if (e instanceof String) {
                flag = "cadena";
            } else if (e instanceof Boolean) {
                flag = "boolean";
            }

        }
        ts.add(new Simbolo(id, Simbolo.Tipo.HETEROGENEO, elements));
        return null;
    }
}
