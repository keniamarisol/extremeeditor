/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Kenia
 */

import java.util.LinkedList;

public class IfTernario implements Instruccion {

    private Instruccion condicion;
    private Instruccion instruccionTrue;
    private Instruccion instruccionElse;
    private Boolean valorCondicion;

    public IfTernario(Instruccion condicion, Instruccion instruccionTrue, Instruccion instruccionElse) {
        this.condicion = condicion;
        this.instruccionTrue = instruccionTrue;
        this.instruccionElse = instruccionElse;
    }

    @Override
    public Object ejecutar(TablaDeSimbolos ts, Arbol ar) {
        valorCondicion = ((condicion == null) ? false : (Boolean) condicion.ejecutar(ts, ar));
        if (valorCondicion) {
            TablaDeSimbolos tablaLocal = new TablaDeSimbolos();
            tablaLocal.addAll(ts);
            Object r;
            r = instruccionTrue.ejecutar(tablaLocal, ar);
            if (r != null) {
                return r;
            }

        } else {
            TablaDeSimbolos tablaLocal = new TablaDeSimbolos();
            tablaLocal.addAll(ts);
            Object r;
            r = instruccionElse.ejecutar(tablaLocal, ar);
            if (r != null) {
                return r;
            }
        }
        return null;
    }
}
