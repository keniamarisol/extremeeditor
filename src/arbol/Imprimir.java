/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Kenia
 */


public class Imprimir implements Instruccion{
    
    private final Instruccion contenido;
    
    public Imprimir(Instruccion contenido) {
        this.contenido = contenido;
    }
    
    @Override
    public Object ejecutar(TablaDeSimbolos ts,Arbol ar) {
        Object rImprimir=contenido.ejecutar(ts,ar);//Variable que almacena el resultado de la expresión que se desea imprimir
        if(rImprimir != null){
            SingletonArbol.getInstance().AstSalida.add(String.valueOf(rImprimir));
        }
        return null;
    }
    
}
