
package arbol;

import java.util.LinkedList;

public class Simbolo {

    private boolean parametro;

    private boolean parametroInicializado;

    private final Tipo tipo;

    private final String id;

    private final LinkedList<Object> elementos;

    private Object valor;    
  
    public Simbolo(String id, Tipo tipo) {
        this.tipo = tipo;
        this.id = id;
        this.elementos = null;
        this.parametro=false;
        this.parametroInicializado=false;
    }

    public Simbolo(String id, Tipo tipo, LinkedList<Object> td) {
        this.tipo = tipo;
        this.id = id;
        this.elementos = td;
        NodoArreglo arr=new NodoArreglo();
        //arr.inicializarNodo(elementos.size(), 1, elementos);
        this.valor=arr;
    }

    public String getId() {
        return id;
    }

    public Object getValor() {
        return valor;
    }

    void setValor(Object v) {
        //Variable que alberga el tipo del valor a asignar
        Tipo vTipo=null;
        /*if (v instanceof Double) {
            vTipo = Simbolo.Tipo.NUMERO;
        } else if(v instanceof String) {
            vTipo = Simbolo.Tipo.CADENA;
        } else if(v instanceof Boolean){
            vTipo = Simbolo.Tipo.BOOLEANO;
        }*/
        //¿Es el valor a asignar igual al tipo de la variable a la que se le va a asignar el valor?
       /* if(vTipo == tipo){
            valor=v;
        }else{
            SingletonArbol.getInstance().AstErrores.add("Esta intentando asignar un valor de tipo ["+(vTipo==null?"null":vTipo.name())+"] a la variable ["+id+"] de tipo ["+tipo.name()+ "], esto no está permitido.");
        }*/
       
       valor=v;
    }

    void setValor(Object val, LinkedList<Integer> indices) {
        if(this.valor instanceof NodoArreglo){
            if(this.elementos.size()==indices.size()){
                NodoArreglo arr=(NodoArreglo)this.valor;
                arr.setValor(indices.size(), 1, indices, val, id);
            }else{
                SingletonArbol.getInstance().AstSalida.add("La cantidad de indices indicados no "
                        + "coincide con la cantidad de dimensiones del arreglo "+id+", no puede asignársele un valor.");            
            }
        }else{
            SingletonArbol.getInstance().AstSalida.add("La variable "+id+" no es un arreglo, por lo "
                    + "que no puede asignársele un valor de esta manera.");
        }
    }

    Object getValor(String id, LinkedList<Integer> indices) {
        if(this.valor instanceof NodoArreglo){
            if(this.elementos.size()==indices.size()){
                NodoArreglo arr=(NodoArreglo)this.valor;
                return arr.getValor(indices.size(), 1, indices, id);
            }else{
                SingletonArbol.getInstance().AstSalida.add("La cantidad de indices indicados no "
                        + "coincide con la cantidad de dimensiones del arreglo "+id+", no puede accederse a este arreglo.");            
            }
        }else{
            SingletonArbol.getInstance().AstSalida.add("La variable "+id+" no es un arreglo, por lo "
                    + "que no se puede accesar de esta manera.");
        }
        return null;
    }
 
    public static enum Tipo{
        NUMERO,
        CADENA,
        BOOLEANO,
        VAR,
        HETEROGENEO,
        VOID
    }

    public boolean isParametro() {
        return parametro;
    }

    public void setParametro(boolean parametro) {
        this.parametro = parametro;
    }

    public boolean isParametroInicializado() {
        return parametroInicializado;
    }

    public void setParametroInicializado(boolean parametroInicializado) {
        this.parametroInicializado = parametroInicializado;
    }
}
