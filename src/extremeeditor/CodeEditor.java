/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extremeeditor;

import arbol.Arbol;
import arbol.SingletonArbol;
import arbol.TablaDeSimbolos;
import gramaticaGXML.Interprete;
import java.awt.Color;
import java.io.Reader;
import java.io.StringReader;
import java.util.Iterator;
import javax.swing.*;
import org.fife.ui.rtextarea.*;
import org.fife.ui.rsyntaxtextarea.*;

/**
 *
 * @author Kenia
 */
public class CodeEditor extends javax.swing.JPanel {

    RSyntaxTextArea editor = new RSyntaxTextArea(945, 395);
    RSyntaxTextArea consola = new RSyntaxTextArea(945, 300);

    /**
     * Creates new form codeEditor
     */
    public CodeEditor() {
        initComponents();

        editor.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVA);
        editor.setCodeFoldingEnabled(true);
        RTextScrollPane sp = new RTextScrollPane(editor);
        RTextScrollPane sp2 = new RTextScrollPane(consola);
        jPanelEditor.add(sp);

        // consola.setForeground(Color.lightGray);
        consola.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_WINDOWS_BATCH);
        consola.setCodeFoldingEnabled(true);
        consola.setBackground(Color.DARK_GRAY);
        consola.setText("Salida.... ");
        jPanelConsola.add(sp2);
    }

    public String getText() {
        return editor.getText();
    }

    public void setText(String text) {
        editor.setText(text);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonCompilar = new javax.swing.JButton();
        jPanelEditor = new javax.swing.JPanel();
        jPanelConsola = new javax.swing.JPanel();

        setAutoscrolls(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMaximumSize(new java.awt.Dimension(950, 400));
        setMinimumSize(new java.awt.Dimension(950, 400));
        setPreferredSize(new java.awt.Dimension(950, 400));

        jButtonCompilar.setText("Compilar");
        jButtonCompilar.setPreferredSize(new java.awt.Dimension(20, 32));
        jButtonCompilar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCompilarActionPerformed(evt);
            }
        });

        jPanelEditor.setPreferredSize(new java.awt.Dimension(945, 350));
        jPanelEditor.setLayout(new java.awt.GridLayout(1, 0));

        jPanelConsola.setBackground(new java.awt.Color(0, 0, 0));
        jPanelConsola.setForeground(new java.awt.Color(204, 204, 204));
        jPanelConsola.setAutoscrolls(true);
        jPanelConsola.setLayout(new java.awt.GridLayout(1, 0));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButtonCompilar, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanelEditor, javax.swing.GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jPanelConsola, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jButtonCompilar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelEditor, javax.swing.GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanelConsola, javax.swing.GroupLayout.PREFERRED_SIZE, 263, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getAccessibleContext().setAccessibleParent(this);
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCompilarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCompilarActionPerformed
        // TODO add your handling code here:
        gramaticaFS.Lexico lexFs = null;
        gramaticaFS.Sintactico miParserFs = null;
        Arbol AST_arbolSintaxisAbstracta = null;

        gramaticaGXML.Lexico lexGxml = null;
        gramaticaGXML.Sintactico miParserGxml = null;
        String fScript = "";

        Interprete interGxml = null;
        try {
            String entrada = editor.getText();
            Reader reader = new StringReader(entrada);
            lexGxml = new gramaticaGXML.Lexico(reader);
            miParserGxml = new gramaticaGXML.Sintactico(lexGxml);
            miParserGxml.parse();
            System.out.println(lexGxml.tokensList.toString());
            
            graficar.graficarArbolGXML graficar = new graficar.graficarArbolGXML();
            graficar.grafico(miParserGxml.nodoRaiz);
            interGxml = new Interprete();
            fScript = interGxml.iniciar(miParserGxml.nodoRaiz);
            

        } catch (Exception ex) {
            System.out.println("Error al Compilar " + ex.toString());

        }

        consola.setText("");

        consola.append("\n");

        for (String error : lexGxml.tokensListErrores) {
            consola.append(error + "\n");
        }

        for (String error : miParserGxml.ParserListErrores) {
            consola.append(error + "\n");
        }
        
        consola.append("*************Código fScript*************\n\n" + fScript);
        
        consola.append("*************Analizador GXML Finalizado*************\n\n");

        try {
            Reader reader = new StringReader(fScript);
            lexFs = new gramaticaFS.Lexico(reader);
            miParserFs = new gramaticaFS.Sintactico(lexFs);
            miParserFs.parse();
            System.out.println(lexFs.tokensList.toString());
            
            AST_arbolSintaxisAbstracta = miParserFs.getAST();

        } catch (Exception ex) {

            System.out.println("Error al Compilar " + ex.toString());

        }

        for (String error : lexFs.tokensListErrores) {
            consola.append(error + "\n");
        }

        for (String error : miParserFs.ParserListErrores) {
            consola.append(error + "\n");
        }

        if (AST_arbolSintaxisAbstracta == null) {
            System.err.println("No es posible ejecutar las instrucciones porque\r\n"
                    + "el árbol no fue cargado de forma adecuada por la existencia\r\n"
                    + "de errores léxicos o sintácticos.");

        } else {
            //Se crea una tabla de símbolos global para ejecutar las instrucciones.
            TablaDeSimbolos ts = new TablaDeSimbolos();
            //Se ejecuta el árbol
            AST_arbolSintaxisAbstracta.ejecutar(ts, AST_arbolSintaxisAbstracta);
            SingletonArbol.getInstance().AstErrores.forEach((error) -> {
                consola.append(error + "\n");
            });

            SingletonArbol.getInstance().AstSalida.forEach((salida) -> {
                consola.append(salida + "\n");
            });

            consola.append("\n*************Tabla De Símbolos*************\n");

            ts.forEach((sim) -> {
                consola.append(" | ID: " + sim.getId() + " | VALOR: " + sim.getValor() + " | \n");
            });

        }
        
        consola.append("\n*************Analizador FSCRIPT Finalizado*************\n");


    }//GEN-LAST:event_jButtonCompilarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCompilar;
    private javax.swing.JPanel jPanelConsola;
    private javax.swing.JPanel jPanelEditor;
    // End of variables declaration//GEN-END:variables
}
