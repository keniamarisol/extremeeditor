
package arbol;

import java.util.LinkedList;


public class AccesoArreglo implements Instruccion{
    
    private final LinkedList<Instruccion> indices;
    
    protected final String id;
    
    public AccesoArreglo(String id, LinkedList<Instruccion> indices) {
        this.indices = indices;
        this.id = id;
    }
        
    @Override
    public Object ejecutar(TablaDeSimbolos ts, Arbol ar) {
        LinkedList<Integer> valoresIndices=new LinkedList<>();
        for (Instruccion dim : indices) {
            Object er=dim.ejecutar(ts, ar);

            if(!(er instanceof Double)){
                SingletonArbol.getInstance().AstErrores.add("Los indices para acceder a un arreglo deben ser de tipo numérico. El indice ["+String.valueOf(er)+"] no es numérico.");
                return null;
            }
            valoresIndices.add(((Double)er).intValue());
        }
        return ts.getValor(id, valoresIndices);
    }
    
}
