/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Kenia
 */

import java.util.LinkedList;

public class Function implements Instruccion {
  
    private final Simbolo.Tipo tipo;

    private final String identificador;
 
    private final LinkedList<Declaracion> parametros;
  
    private LinkedList<Instruccion> valoresParametros;
 
    private final LinkedList<Instruccion> instrucciones;

    public Function(String a, String b, LinkedList<Declaracion> c, LinkedList<Instruccion> d) {
        switch (a.toLowerCase()) {
            case "number": tipo=Simbolo.Tipo.NUMERO;
                     break;
            case "string":  tipo=Simbolo.Tipo.CADENA;
                     break;
            case "boolean": tipo=Simbolo.Tipo.BOOLEANO;
                     break;
            case "void": tipo=Simbolo.Tipo.VOID;
                     break;
            case "var": tipo=Simbolo.Tipo.VAR;
                     break;
            default:
                tipo=null;
        }
        identificador=b;
        parametros=c;
        instrucciones=d;
    }

    public Function(String a, String b, LinkedList<Instruccion> c) {
        String reservadaTipo=a.toLowerCase();
        switch (reservadaTipo) {
            case "number": tipo=Simbolo.Tipo.NUMERO;
                     break;
            case "string":  tipo=Simbolo.Tipo.CADENA;
                     break;
            case "boolean": tipo=Simbolo.Tipo.BOOLEANO;
                     break;
            case "void": tipo=Simbolo.Tipo.VOID;
                     break;
            case "var": tipo=Simbolo.Tipo.VAR;
                     break;
            default:
                tipo=null;
        }
        identificador=b;
        parametros=new LinkedList<>();
        instrucciones=c;
    }

    @Override
    public Object ejecutar(TablaDeSimbolos ts,Arbol ar) {
        TablaDeSimbolos tablaLocal=new TablaDeSimbolos(); // Creamos una nueva tabla local para la función.
        tablaLocal.addAll(ar.tablaDeSimbolosGlobal); // Agregamos a la tabla local las referencias a las variables globales.
        if(parametros.size()==valoresParametros.size()){
            for(int i=0;i<parametros.size();i++){
                
                Declaracion d=parametros.get(i); 
                d.setParametro(true);
                d.ejecutar(tablaLocal, ar);
                
                Asignacion a=new Asignacion(d.getIdentificador(), valoresParametros.get(i));
                a.setTablaDeSimbolosPadre(ts); // Indicamos que los valores de los parámetros los debe obtener de la tabla padre de esta función.
                
                a.ejecutar(tablaLocal, ar);
                tablaLocal.setParametroInicializado(d.getIdentificador());
            }
            for(Instruccion ins:instrucciones){
                Object r;
                r=ins.ejecutar(tablaLocal,ar);
                if(r!=null){
                    return r;
                }
            }            
        }else{
            SingletonArbol.getInstance().AstErrores.add("La cantidad de parámetros enviada a la función " + identificador + " no es correcta.");
        }
        return null;
    }

    public String getIdentificador() {

        String id = "_" + identificador + "(";
        if (parametros != null) {
            for (Declaracion parametro: parametros) {
                id += "_" + parametro.tipo.name();
            }
        }
        id += ")";
        
        return id.toLowerCase();
    }

    public void setValoresParametros(LinkedList<Instruccion> a) {
        valoresParametros=a;
    }

    public Simbolo.Tipo getTipo() {
        return tipo;
    }
    
}
