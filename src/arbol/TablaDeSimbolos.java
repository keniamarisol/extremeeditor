
package arbol;

import java.util.LinkedList;


public class TablaDeSimbolos extends LinkedList<Simbolo>{
 
    public TablaDeSimbolos() {
        super();
    }

    Object getValor(String id) {
        for (int i = this.size()-1; i >= 0; i--) {
            Simbolo s=this.get(i);
            if(s.isParametro() && s.isParametroInicializado() || !s.isParametro()){
                if(s.getId().equals(id)){
                    Object aux=s.getValor();
                    return aux;
                }
            }
        }
        SingletonArbol.getInstance().AstErrores.add("La variable "+id+" no existe en este ámbito.");
        return null;
    }
  
    Object getValor(String id, LinkedList<Integer> indices) {
        for(Simbolo s:this){
            if(s.getId().equals(id)){
                return s.getValor(id, indices);
            }
        }
        SingletonArbol.getInstance().AstSalida.add("La variable "+id+" no existe en este ámbito.");
        return null;
    }

    void setValor(String id, Object valor) {
        for (int i = this.size()-1; i >= 0; i--) {
            Simbolo s=this.get(i);
            if(s.getId().equals(id)){
                s.setValor(valor);
                return;
            }
        }
        SingletonArbol.getInstance().AstSalida.add("La variable "+id+" no existe en este ámbito, por lo "
                + "que no puede asignársele un valor.");
    }
  
    void setValor(String id, Object valor, LinkedList<Integer> indices) {
        for(Simbolo s:this){
            if(s.getId().equals(id)){
                s.setValor(valor,indices);
                return;
            }
        }
        SingletonArbol.getInstance().AstSalida.add("La variable "+id+" no existe en este ámbito, por lo "
                + "que no puede asignársele un valor.");
    }
 
    void setParametroInicializado(String id) {
        for (int i = this.size()-1; i >= 0; i--) {
            Simbolo s=this.get(i);
            if(s.getId().equals(id)){
                s.setParametroInicializado(true);
                return;
            }
        }
        SingletonArbol.getInstance().AstSalida.add("El parámtro "+id+" que quiere marcar como inicializado no existe en este ámbito, por lo "
                + "que no puede marcar.");
    }
}