/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Kenia
 */
import java.util.LinkedList;

public class CrearVentana implements Instruccion {

    private final String identificador = "";

    private final LinkedList<Instruccion> elementos;

    public CrearVentana(LinkedList<Instruccion> b) {
        // identificador = a;
        elementos = b;
    }

    @Override
    public Object ejecutar(TablaDeSimbolos ts, Arbol ar) {

        String id = null;
        String color = "#000000";
        int alto = 800;
        int ancho = 1200;

        Object oId = elementos.get(3).ejecutar(ts, ar);
        Object oColor = elementos.get(0).ejecutar(ts, ar);
        Object oAlto = elementos.get(1).ejecutar(ts, ar);
        Object oAncho = elementos.get(2).ejecutar(ts, ar);

        if (oId != null && oId instanceof String) {
            id = (String) oId;
        } else {
            SingletonArbol.getInstance().AstErrores.add("El id de la Ventana no es válido");
            return null;
        }

        if (oColor != null && oColor instanceof String) {
            color = (String) oColor;
        } else {
            SingletonArbol.getInstance().AstErrores.add("El color de la Ventana " + id + " no es válido se asignará valor por defecto");
           
        }

        if (oAlto != null && oAlto instanceof String) {
            alto = (Integer) oAlto;
        } else {
            SingletonArbol.getInstance().AstErrores.add("El alto de la Ventana " + id + " no es válido se asignará valor por defecto");
            
        }

        if (oAncho != null && oAncho instanceof String) {
            ancho = (Integer) oAncho;
        } else {
            SingletonArbol.getInstance().AstErrores.add("El ancho de la Ventana " + id + " no es válido se asignará valor por defecto");
            
        }

       // Ventana ven = new Ventana(color, alto, ancho, id);

        return null;
    }
}
