/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Kenia
 */

import java.util.LinkedList;

public class LlamadaFuncion implements Instruccion{

    private final String identificador;

    private final LinkedList<Instruccion> parametros;

    public LlamadaFuncion(String a, LinkedList<Instruccion> b) {
        identificador=a;
        parametros=b;
    }

    @Override
    public Object ejecutar(TablaDeSimbolos ts,Arbol ar) {
 
        // para llamar a la función es necesario construir su identificador único
        String id = "_" + identificador + "(";
        for(Instruccion parametro: parametros) {
            // es necesario evaluar los parametros de la función para saber sus tipos
            // y así poder completar el id
            Object resultado = parametro.ejecutar(ts, ar);
            
             id += "_" + Simbolo.Tipo.VAR;
            
           /* if (resultado instanceof Double) {
                id += "_" + Simbolo.Tipo.NUMERO;
            } else if(resultado instanceof String) {
                id += "_" + Simbolo.Tipo.CADENA;
            } else if(resultado instanceof Boolean){
                id += "_" + Simbolo.Tipo.BOOLEANO;
            }*/
        }
        id += ")";
        
        Function f=ar.getFunction(id.toLowerCase());
        if(null!=f){
            f.setValoresParametros(parametros);
            Object rFuncion=f.ejecutar(ts, ar); //Objeto que almacena el resultado de la ejecución del proceso
            if(f.getTipo()==Simbolo.Tipo.VOID && !(rFuncion instanceof Return || rFuncion == null)){
                SingletonArbol.getInstance().AstErrores.add("Una función de tipo Void no puede retornar valores, solamente puede retornar vacío.");
                return null;
            }else if(f.getTipo()==Simbolo.Tipo.NUMERO && !(rFuncion instanceof Double)){
                SingletonArbol.getInstance().AstErrores.add("Una función de tipo Number no puede retornar un valor que no sea numérico.");
                return null;
            }else if(f.getTipo()==Simbolo.Tipo.BOOLEANO && !(rFuncion instanceof Boolean)){
                SingletonArbol.getInstance().AstErrores.add("Una función de tipo Boolean no puede retornar un valor que no sea verdadero o falso.");
                return null;
            }else if(f.getTipo()==Simbolo.Tipo.CADENA && !(rFuncion instanceof String)){
                SingletonArbol.getInstance().AstErrores.add("Una función de tipo Cadena no puede retornar un valor que no sea una cadena de caracteres.");
                return null;
            }else{
            
            }
            
            if(rFuncion instanceof Return){
                return null;
            }else{
                return rFuncion;
            }
        } else {
            SingletonArbol.getInstance().AstErrores.add("La función " + identificador + " no existe.");    
        }
        return null;
    }
}
