/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Kenia
 */

import java.util.LinkedList;


public class NodoArreglo {
    
    private final LinkedList<NodoArreglo> celdasVecinas;
    
    private Object valor;
    
    public NodoArreglo() {
        this.celdasVecinas = new LinkedList<>();
        this.valor = null;
    }
    
    public void inicializarNodo(int cantDimensiones, int dimensionActual, LinkedList<Integer> tamaniosDimensiones){
        if(dimensionActual>cantDimensiones){
            return;
        }
        for (int i = 0; i < tamaniosDimensiones.get(dimensionActual-1) ; i++) {
            NodoArreglo arr=new NodoArreglo();
            celdasVecinas.add(arr);
            arr.inicializarNodo(cantDimensiones, dimensionActual+1, tamaniosDimensiones);            
        }
    }
    
    public void setValor(int cantIndices, int indiceActual, LinkedList<Integer> indices, Object val, String id){
        int valIndiceActual=indices.get(indiceActual-1);
        if(valIndiceActual<celdasVecinas.size() && valIndiceActual>=0){
            NodoArreglo arr=celdasVecinas.get(valIndiceActual);
            if(indiceActual==cantIndices){
                arr.valor=val;
            }else{
                arr.setValor(cantIndices, indiceActual+1, indices, val, id);
            }
        }else{
            SingletonArbol.getInstance().AstErrores.add("La asignación al arreglo "+id+" no puede "
                    + "realizarse porque uno o más de los indices exceden "
                    + "los límites del arreglo.");
        }
    }
    
    Object getValor(int cantIndices, int indiceActual, LinkedList<Integer> indices, String id) {
        int valIndiceActual=indices.get(indiceActual-1);
        if(valIndiceActual<celdasVecinas.size() && valIndiceActual>=0){
            NodoArreglo arr=celdasVecinas.get(valIndiceActual);
            if(indiceActual==cantIndices){
                return arr.valor;
            }else{
                return arr.getValor(cantIndices, indiceActual+1, indices, id);
            }
        }else{
            SingletonArbol.getInstance().AstErrores.add("El acceso al arreglo "+id+" no puede "
                    + "realizarse porque uno o más de los indices exceden "
                    + "los límites del arreglo.");
        }
        return null;
    }
}
