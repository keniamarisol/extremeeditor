/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

import java.util.ArrayList;

/**
 *
 * @author Kenia
 */
public class SingletonArbol {
    
    public  ArrayList<String> AstErrores = new ArrayList<>();
    public  ArrayList<String> AstSalida = new ArrayList<>();
    public int contador_break = 0;
    
    private SingletonArbol() {
    }
    
    public static SingletonArbol getInstance() {
        return SingletonHolder.INSTANCE;
    }
    
    private static class SingletonHolder {

        private static final SingletonArbol INSTANCE = new SingletonArbol();
    }
}
