/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Kenia
 */
import java.util.LinkedList;

public class Arbol implements Instruccion {

    private final LinkedList<Instruccion> instrucciones;

    public TablaDeSimbolos tablaDeSimbolosGlobal;

    public Arbol(LinkedList<Instruccion> a) {
        instrucciones = a;
    }

    @Override
    public Object ejecutar(TablaDeSimbolos ts, Arbol ar) {

        tablaDeSimbolosGlobal = ts;

        for (Instruccion ins : instrucciones) {
            if (ins instanceof Declaracion) {
                Declaracion d = (Declaracion) ins;
                d.ejecutar(ts, ar);
            }else  if (ins instanceof DeclaAsig) {
                DeclaAsig d = (DeclaAsig) ins;
                d.ejecutar(ts, ar);
            }else if (ins instanceof Function) {
                Function f = (Function) ins;
                String id = f.getIdentificador();
                // el identificador único de la función main es _main() ya que no recibe parámetros
                if ("_main()".equals(id)) {
                    f.setValoresParametros(new LinkedList<>());
                    f.ejecutar(ts, ar);
                    break;
                }
            }else{
             ins.ejecutar(ts, ar);
            }
        }
       /* for (Instruccion ins : instrucciones) {
            if (ins instanceof Function) {
                Function f = (Function) ins;
                String id = f.getIdentificador();
                // el identificador único de la función main es _main() ya que no recibe parámetros
                if ("_main()".equals(id)) {
                    f.setValoresParametros(new LinkedList<>());
                    f.ejecutar(ts, ar);
                    break;
                }
            }
        }*/
        return null;
    }

    public Function getFunction(String identificador) {
        for (Instruccion ins : instrucciones) {
            if (ins instanceof Function) {
                Function f = (Function) ins;
                String id = f.getIdentificador();
                if (identificador.equals(id)) {
                    return f;
                }
            }
        }
        return null;
    }

}
