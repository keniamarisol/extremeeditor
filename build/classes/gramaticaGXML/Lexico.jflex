package gramaticaGXML;
import java_cup.runtime.Symbol; 
import java.util.ArrayList;

%% 
%class Lexico
%cupsym TablaSimbolos
%function next_token
%type java_cup.runtime.Symbol
%public 
%line 
%char 
%cup 
%unicode
%ignorecase

%init{ 
    yyline = 1; 
    yychar = 1; 

 /* The %init directive allows us to introduce code in the class constructor. We are using it for initializing our variables */
this.tokensList = new ArrayList();
this.tokensListErrores = new ArrayList();

%init}

%{

public static ArrayList tokensList;
public static ArrayList<String> tokensListErrores = new ArrayList<>();;
%}

 
//BLANCOS=[ \r\t]+
CADENACOMILLASDOBLES = [\"]([^\"\n]|(\\\"))*[\"]
CADENALLAVES = [{]([^\{\n]|(\\\}))*[}]
//INPUT = [\>]([^\n\<]|(\\\<) | (\\\>))*[\<][\/]

ENTERO=[0-9]+
DECIMAL=[0-9]+("."[  |0-9]+)?
ID=[A-Za-z]+["_""."0-9A-Za-z]*
COMENTMULTILINEA   = "#$" [^$] ~"$#" | "#$" "$"+ "$#"
COMENTUNILINEA     = "##" {InputCharacter}* {LineTerminator}?
LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
WhiteSpace     = {LineTerminator} | [ \t\f]
INPUT = ([^" "\"\<\>\=\r\n\t\f])+ 
//INPUT = ">"~"<\\"  ;
%%

{COMENTUNILINEA} {} 
{COMENTMULTILINEA} {} 
{CADENALLAVES} {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.CADENALLAVES,yyline,yychar, yytext()); } 

{CADENACOMILLASDOBLES} {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.CADENA,yyline,yychar, (yytext()).substring(1,yytext().length()-1)); } 



"importar" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TIMPORTAR,yyline,yychar, yytext()); } 
"ventana" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TVENTANA,yyline,yychar, yytext()); } 
"contenedor" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TCONTENEDOR,yyline,yychar, yytext()); } 
"texto" {this.tokensList.add("Token: "+yytext());return new Symbol(TablaSimbolos.TTEXTO,yyline,yychar, yytext()); } 
"control" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TCONTROL,yyline,yychar, yytext()); } 
"defecto" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TDEFECTO,yyline,yychar, yytext()); } 
"listadatos" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TLISTADATOS,yyline,yychar, yytext()); } 
"dato" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TDATO,yyline,yychar, yytext()); } 
"multimedia" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TMULTIMEDIA,yyline,yychar, yytext()); } 
"boton" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TBOTON,yyline,yychar, yytext()); } 
"enviar" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TENVIAR,yyline,yychar, yytext()); } 
"verdadero" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TVERDADERO,yyline,yychar, yytext()); } 


"</" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.FIN,yyline,yychar, yytext()); } 
"<" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MENQUE,yyline,yychar, yytext());} 
">" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MAYQUE,yyline,yychar, yytext());} 
"=" {this.tokensList.add("SignoIgual: "+yytext()); return new Symbol(TablaSimbolos.IGUAL,yyline,yychar, yytext());} 

{ID} {this.tokensList.add("ID: "+yytext()); return new Symbol(TablaSimbolos.ID,yyline,yychar, yytext()); } 
{ENTERO} {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.ENTERO,yyline,yychar, yytext()); } 
{DECIMAL} {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.DECIMAL,yyline,yychar, yytext()); } 
{INPUT} {this.tokensList.add("INPUT: "+yytext()); return new Symbol(TablaSimbolos.INPUT,yyline,yychar, yytext()); } 



{WhiteSpace} {/*seignoran*/}

. {
    System.err.println("Este es un error lexico: "+yytext()+", en la linea: "+yyline+", en la columna: "+yychar);
    this.tokensListErrores.add("Este es un error lexico: "+yytext()+", en la linea: "+yyline+", en la columna: "+yychar);

}