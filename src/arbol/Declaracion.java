/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Kenia
 */

import java.util.LinkedList;


public class Declaracion implements Instruccion {

    
    protected boolean parametro;
    
    LinkedList<String> ids = new LinkedList<String>();
    String id = "";
    
    protected final Simbolo.Tipo tipo;

    
    public Declaracion(LinkedList<String> a) {
        tipo = Simbolo.Tipo.VAR;
        ids = a;
        parametro = false;
    }

    public Declaracion(String a) {
        tipo = Simbolo.Tipo.VAR;
        id = a;
        parametro = false;
    }

    
    @Override
    public Object ejecutar(TablaDeSimbolos ts, Arbol ar) {
        if (!ids.isEmpty()) {

            for (String id : ids) {
                Simbolo aux = new Simbolo(id, tipo);
                aux.setParametro(this.parametro);
                ts.add(aux);
            }

        }else
        {
            Simbolo aux = new Simbolo(id, tipo);
            aux.setParametro(this.parametro);
            ts.add(aux);
        }

        return null;
    }

    
    public String getIdentificador() {
        return id;
    }

    
    public boolean isParametro() {
        return parametro;
    }

    
    public void setParametro(boolean parametro) {
        this.parametro = parametro;
    }

}
