/*
 * 
 * 
 * 
 */
package arbol;

/**
 * Clase de retorno.
 * @author Kenia
 */
public class Return implements Instruccion{

    private final Instruccion valorRetorno;

    public Return(Instruccion a) {
        valorRetorno=a;
    }

    public Return() {
        valorRetorno=null;
    }
 
    @Override
    public Object ejecutar(TablaDeSimbolos ts, Arbol ar) {
        if(valorRetorno==null){
            return this;
        }else{
            return valorRetorno.ejecutar(ts, ar);
        }
    }
    
}
