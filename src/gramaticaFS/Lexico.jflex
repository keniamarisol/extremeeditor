package gramaticaFS;
import java_cup.runtime.Symbol; 
import java.util.ArrayList;

%% 
%class Lexico
%cupsym TablaSimbolos
%function next_token
%type java_cup.runtime.Symbol
%public 
%line 
%column
%char 
%cup 
%unicode
%ignorecase

%init{ 
    yyline = 1; 
    yycolumn = 1; 

 /* The %init directive allows us to introduce code in the class constructor. We are using it for initializing our variables */
this.tokensList = new ArrayList();
this.tokensListErrores = new ArrayList();

%init}

%{

public static ArrayList tokensList ;
public static ArrayList<String> tokensListErrores = new ArrayList<>();
%}

 
CADENACOMILLASDOBLES =  [\"]([^\"\n]|(\\\"))*[\"]
NUMERO=[0-9]+("."[  |0-9]+)*
ID=[A-Za-z"_"]+["_"0-9A-Za-z]*
COMENTMULTILINEA   = "//" {InputCharacter}* {LineTerminator}?
COMENTUNILINEA     = "/*" [^*] ~"*/" | "/*" "*"+ "/"
LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
WhiteSpace     = {LineTerminator} | [ \t\f]

%%

{COMENTUNILINEA} {} 
{COMENTMULTILINEA} {} 
{CADENACOMILLASDOBLES} {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.CADENA,yyline,yycolumn, (yytext()).substring(1,yytext().length()-1)); } 



"nulo" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TNULO,yyline,yycolumn, yytext()); } 
"verdadero" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TVERDADERO,yyline,yycolumn, yytext()); } 
"falso" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TFALSO,yyline,yycolumn, yytext()); } 
"var" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TVAR,yyline,yycolumn, yytext()); } 
"imprimir" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TIMPRIMIR,yyline,yycolumn, yytext()); } 
"importar" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TIMPORTAR,yyline,yycolumn, yytext()); }
"detener" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TDETENER,yyline,yycolumn, yytext()); } 
"selecciona" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TSELECCIONA,yyline,yycolumn, yytext()); } 
"caso" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TCASO,yyline,yycolumn, yytext()); } 
"defecto" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TDEFECTO,yyline,yycolumn, yytext()); } 
"retornar" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TRETORNAR,yyline,yycolumn, yytext()); } 
"si" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TSI,yyline,yycolumn, yytext()); } 
"sino" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TSINO,yyline,yycolumn, yytext()); } 
"funcion" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TFUNCION,yyline,yycolumn, yytext()); } 
"ascendete" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TASCENDENTE,yyline,yycolumn, yytext()); } 
"descendente" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TDESCENDENTE,yyline,yycolumn, yytext()); } 
"invertir" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TINVERTIR,yyline,yycolumn, yytext()); } 
"maximo" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TMAXIMO,yyline,yycolumn, yytext()); }  
"minimo" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TMINIMO,yyline,yycolumn, yytext()); } 
"filter" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TFILTER,yyline,yycolumn, yytext()); } 
"buscar" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TBUSCAR,yyline,yycolumn, yytext()); } 
"map" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TMAP,yyline,yycolumn, yytext()); } 
"reduce" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TREDUCE,yyline,yycolumn, yytext()); } 
"todos" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TTODOS,yyline,yycolumn, yytext()); } 
"alguno" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TALGUNO,yyline,yycolumn, yytext()); } 
"CrearVentana" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TCREARVENTANA,yyline,yycolumn, yytext()); } 

//"" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.,yyline,yycolumn, yytext()); } 


"+" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MAS,yyline,yycolumn, yytext());} 
"-" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MENOS,yyline,yycolumn, yytext());} 
"*" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.POR,yyline,yycolumn, yytext());} 
"/" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.DIV,yyline,yycolumn, yytext());} 
"&&" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.AND,yyline,yycolumn, yytext());} 
"||" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.OR,yyline,yycolumn, yytext());} 
"^" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.POTENCIA,yyline,yycolumn, yytext());} 
">" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MAYQUE,yyline,yycolumn, yytext());} 
"<" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MENQUE,yyline,yycolumn, yytext());} 
">=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MAYIGUALQUE,yyline,yycolumn, yytext());} 
"<=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MENIGUALQUE,yyline,yycolumn, yytext());} 
"--" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MENOSMENOS,yyline,yycolumn, yytext());} 
"++" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MASMAS,yyline,yycolumn, yytext());} 
"==" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.IGUALQUE,yyline,yycolumn, yytext());} 
"!=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.DIFQUE,yyline,yycolumn, yytext());} 
"!" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.NOT,yyline,yycolumn, yytext());}
"+=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MASIGUAL,yyline,yycolumn, yytext());} 
"*=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.PORIGUAL,yyline,yycolumn, yytext());} 
"-=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MENOSIGUAL,yyline,yycolumn, yytext());}  
"/=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.DIVIGUAL,yyline,yycolumn, yytext());} 
"=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.IGUAL,yyline,yycolumn, yytext());} 
"(" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.PARIZQ,yyline,yycolumn, yytext());} 
")" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.PARDER,yyline,yycolumn, yytext());} 
"[" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.CORIZQ,yyline,yycolumn, yytext());} 
"]" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.CORDER,yyline,yycolumn, yytext());} 
"{" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.LLAVEIZQ,yyline,yycolumn, yytext());} 
"}" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.LLAVEDER,yyline,yycolumn, yytext());} 
"," {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.COMA,yyline,yycolumn, yytext());} 
";" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.PTCOMA,yyline,yycolumn, yytext());} 
":" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.DOSPUNTOS,yyline,yycolumn, yytext());} 
"." {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.PUNTO,yyline,yycolumn, yytext());} 
"?" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.INTERROGACION,yyline,yycolumn, yytext());} 

{ID} {this.tokensList.add("ID: "+yytext()); return new Symbol(TablaSimbolos.ID,yyline,yycolumn, yytext()); } 
{NUMERO} {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.NUMERO,yyline,yycolumn, yytext()); } 


{WhiteSpace} {/*seignoran*/}

. {
    System.err.println("Este es un error lexico: "+yytext()+", en la linea: "+yyline+", en la columna: "+yycolumn);
    this.tokensListErrores.add("Este es un error lexico: "+yytext()+", en la linea: "+yyline+", en la columna: "+yycolumn);

}