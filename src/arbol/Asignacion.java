package arbol;

public class Asignacion implements Instruccion {

    protected TablaDeSimbolos tablaPadre;

    protected final String id;

    protected final Instruccion valor;

    public Asignacion(String a, Instruccion b) {
        this.id = a;
        this.valor = b;
    }

    public void setTablaDeSimbolosPadre(TablaDeSimbolos ts) {
        this.tablaPadre = ts;
    }

    @Override
    public Object ejecutar(TablaDeSimbolos ts, Arbol ar) {

        if (tablaPadre != null) // Si se definió una tabla padre, se obtendrá de ahí el valor a asignar.
        {
            ts.setValor(id, valor.ejecutar(tablaPadre, ar));
        } else {
            ts.setValor(id, valor.ejecutar(ts, ar));
        }

        return null;
    }

}
