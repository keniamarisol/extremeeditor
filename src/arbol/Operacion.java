package arbol;


public class Operacion implements Instruccion {

 
    public static enum Tipo_operacion {
        SUMA,
        RESTA,
        MULTIPLICACION,
        DIVISION,
        NEGATIVO,
        NUMERO,
        IDENTIFICADOR,
        CADENA,
        MAYOR_QUE,
        MENOR_QUE,
        MAYOR_IGUAL_QUE,
        MENOR_IGUAL_QUE,
        DIFERENTE_QUE,
        IGUAL_QUE,
        NOT,
        AND,
        OR,
        TRUE,
        FALSE,
        POTENCIA,
        CONCATENACION
    }

    private final Tipo_operacion tipo;
 
    private Instruccion operadorIzq;

    private Instruccion operadorDer;
 
    private Object valor;


    public Operacion(Instruccion operadorIzq, Instruccion operadorDer, Tipo_operacion tipo) {
        this.tipo = tipo;
        this.operadorIzq = operadorIzq;
        this.operadorDer = operadorDer;
    }


    public Operacion(Instruccion operadorIzq, Tipo_operacion tipo) {
        this.tipo = tipo;
        this.operadorIzq = operadorIzq;
    }

  
    public Operacion(String a, Tipo_operacion tipo) {
        this.valor = a;
        this.tipo = tipo;
    }

    public Operacion(Double a) {
        this.valor = a;
        this.tipo = Tipo_operacion.NUMERO;
    }

   
    @Override
    public Object ejecutar(TablaDeSimbolos ts, Arbol ar) {
        Object op1 = new Object(), op2 = new Object(), opU = new Object();

        op1 = (operadorIzq == null) ? null : operadorIzq.ejecutar(ts, ar);
        op2 = (operadorDer == null) ? null : operadorDer.ejecutar(ts, ar);

        switch (tipo) {
            case SUMA:
                //Tipo resultante de datos: Decimal
                if (op1 instanceof Integer && op2 instanceof Double) {
                    return (Integer) op1 + (Double) op2;
                } else if (op1 instanceof Double && op2 instanceof Integer) {
                    return (Double) op1 + (Integer) op2;
                } else if (op1 instanceof Double && op2 instanceof Character) {
                    return (Double) op2 + (int) ((char) (op2));
                } else if (op1 instanceof Character && op2 instanceof Double) {
                    return (int) ((char) (op1)) + (Double) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Double) {
                    int o1 = (Boolean) op1 ? 1 : 0;
                    return o1 + (Double) op2;
                } else if (op1 instanceof Double && op2 instanceof Boolean) {
                    int o2 = (Boolean) op1 ? 1 : 0;
                    return (Double) op1 + o2;
                } else if (op1 instanceof Double && op2 instanceof Double) {
                    return (Double) op1 + (Double) op2;
                } //Tipo resultante de datos: Entero
                else if (op1 instanceof Integer && op2 instanceof Character) {
                    return (Integer) op1 + (int) ((Character) op2);
                } else if (op1 instanceof Character && op2 instanceof Integer) {
                    return (int) ((Character) op1) + (Integer) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Integer) {
                    int o1 = (Boolean) op1 ? 1 : 0;
                    return o1 + (Integer) op2;
                } else if (op1 instanceof Integer && op2 instanceof Boolean) {
                    int o2 = (Boolean) op1 ? 1 : 0;
                    return (Integer) op1 + o2;
                } else if (op1 instanceof Integer && op2 instanceof Integer) {
                    return (Integer) op1 + (Integer) op2;
                } //Tipo resultante de datos: Cadena
                else if (op1 instanceof String || op2 instanceof String) {
                    return op1.toString() + op2.toString();
                } //Tipo resultante de datos: Bool
                else if (op1 instanceof Boolean && op2 instanceof Boolean) {
                    return (Boolean) op1 || (Boolean) op2;
                } else {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos en la suma");
                }
                break;
            case RESTA:
                //Tipo resultante de datos: Decimal
                if (op1 instanceof Integer && op2 instanceof Double) {
                    return (Integer) op1 - (Double) op2;
                } else if (op1 instanceof Double && op2 instanceof Integer) {
                    return (Double) op1 - (Integer) op2;
                } else if (op1 instanceof Double && op2 instanceof Character) {
                    return (Double) op2 - (int) ((char) (op2));
                } else if (op1 instanceof Character && op2 instanceof Double) {
                    return (int) ((char) (op1)) - (Double) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Double) {
                    int o1 = (Boolean) op1 ? 1 : 0;
                    return o1 - (Double) op2;
                } else if (op1 instanceof Double && op2 instanceof Boolean) {
                    int o2 = (Boolean) op1 ? 1 : 0;
                    return (Double) op1 - o2;
                } else if (op1 instanceof Double && op2 instanceof Double) {
                    return (Double) op1 - (Double) op2;
                } //Tipo resultante de datos: Entero
                else if (op1 instanceof Integer && op2 instanceof Character) {
                    return (Integer) op1 - (int) ((Character) op2);
                } else if (op1 instanceof Character && op2 instanceof Integer) {
                    return (int) ((Character) op1) - (Integer) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Integer) {
                    int o1 = (Boolean) op1 ? 1 : 0;
                    return o1 - (Integer) op2;
                } else if (op1 instanceof Integer && op2 instanceof Boolean) {
                    int o2 = (Boolean) op1 ? 1 : 0;
                    return (Integer) op1 - o2;
                } else if (op1 instanceof Integer && op2 instanceof Integer) {
                    return (Integer) op1 - (Integer) op2;
                } //Tipo resultante de datos: Cadena
                else if (op1 instanceof String || op2 instanceof String) {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo el operador" + " menos para dos cadenas");
                    return null;
                } //Tipo resultante de datos: Bool
                else if (op1 instanceof Boolean && op2 instanceof Boolean) {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo el operador" + " menos para dos boolos");
                    return null;
                }
                break;
            case MULTIPLICACION:
                //Tipo resultante de datos: Decimal
                if (op1 instanceof Integer && op2 instanceof Double) {
                    return (Integer) op1 * (Double) op2;
                } else if (op1 instanceof Double && op2 instanceof Integer) {
                    return (Double) op1 * (Integer) op2;
                } else if (op1 instanceof Double && op2 instanceof Character) {
                    return (Double) op2 * (int) ((char) (op2));
                } else if (op1 instanceof Character && op2 instanceof Double) {
                    return (int) ((char) (op1)) * (Double) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Double) {
                    int o1 = (Boolean) op1 ? 1 : 0;
                    return o1 * (Double) op2;
                } else if (op1 instanceof Double && op2 instanceof Boolean) {
                    int o2 = (Boolean) op1 ? 1 : 0;
                    return (Double) op1 * o2;
                } else if (op1 instanceof Double && op2 instanceof Double) {
                    return (Double) op1 * (Double) op2;
                } //Tipo resultante de datos: Entero
                else if (op1 instanceof Integer && op2 instanceof Character) {
                    return (Integer) op1 * (int) ((Character) op2);
                } else if (op1 instanceof Character && op2 instanceof Integer) {
                    return (int) ((Character) op1) * (Integer) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Integer) {
                    int o1 = (Boolean) op1 ? 1 : 0;
                    return o1 * (Integer) op2;
                } else if (op1 instanceof Integer && op2 instanceof Boolean) {
                    int o2 = (Boolean) op1 ? 1 : 0;
                    return (Integer) op1 * o2;
                } else if (op1 instanceof Integer && op2 instanceof Integer) {
                    return (Integer) op1 * (Integer) op2;
                } //Tipo resultante de datos: Cadena
                else if (op1 instanceof String || op2 instanceof String) {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo el operador" + " por para dos cadenas");
                    return null;
                } //Tipo resultante de datos: Bool
                else if (op1 instanceof Boolean && op2 instanceof Boolean) {
                    return (Boolean) op1 && (Boolean) op2;
                }
                break;
            case DIVISION:
                //Tipo resultante de datos: Decimal
                if (op1 instanceof Integer && op2 instanceof Double) {
                    if ((double) op2 != 0.0) {
                        return (int) op1 / (double) op2;
                    } else {
                        SingletonArbol.getInstance().AstErrores.add("Excepcion aritmetica: division(/) por cero");
                        return null;
                    }
                } else if (op1 instanceof Double && op2 instanceof Double) {
                    if ((double) op2 != 0.0) {
                        return (double) op1 / (double) op2;
                    } else {
                        SingletonArbol.getInstance().AstErrores.add("Excepcion aritmetica: division(/) por cero");
                        return null;
                    }
                } //Tipo resultante de datos: Entero
                else if (op1 instanceof Integer && op2 instanceof Character) {
                    if ((int) ((char) (op2)) != 0) {
                        return (int) op1 / (int) ((char) op2);
                    } else {
                        SingletonArbol.getInstance().AstErrores.add("Excepcion aritmetica: division(/) por cero");
                        return null;
                    }
                } else if (op1 instanceof Character && op2 instanceof Integer) {
                    if ((int) op2 != 0) {
                        return (int) ((char) op1) / (int) op2;
                    } else {
                        SingletonArbol.getInstance().AstErrores.add("Excepcion aritmetica: division(/) por cero");
                        return null;
                    }
                } else if (op1 instanceof Boolean && op2 instanceof Integer) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    if ((int) op2 != 0) {
                        return o1 / (int) op2;
                    } else {
                        SingletonArbol.getInstance().AstErrores.add("Excepcion aritmetica: division(/) por cero");
                        return null;
                    }
                } else if (op1 instanceof Integer && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    if (o2 != 0) {
                        return (int) op1 / o2;
                    } else {
                        SingletonArbol.getInstance().AstErrores.add("Excepcion aritmetica: division(/) por cero");
                        return null;
                    }
                } else if (op1 instanceof Integer && op2 instanceof Integer) {
                    if ((int) op2 != 0) {
                        return (int) op1 / (int) op2;
                    } else {
                        SingletonArbol.getInstance().AstErrores.add("Excepcion aritmetica: division(/) por cero");
                        return null;
                    }
                } //Tipo resultante de datos: Cadena
                else if (op1 instanceof String || op2 instanceof String) {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo el operador" + " division para dos cadenas");
                    return null;
                } //Tipo resultante de datos: Bool
                else if (op1 instanceof Boolean && op2 instanceof Boolean) {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo el operador" + " division para dos boolos");
                    return null;
                }
                break;
            case POTENCIA:
                //Tipo resultante de datos: Decimal
                if (op1 instanceof Integer && op2 instanceof Double) {
                    return Math.pow((int) op1, (double) op2);
                } else if (op1 instanceof Double && op2 instanceof Integer) {
                    return Math.pow((double) op1, (int) op2);
                } else if (op1 instanceof Double && op2 instanceof Character) {
                    return Math.pow((double) op2, (int) ((char) (op2)));
                } else if (op1 instanceof Character && op2 instanceof Double) {
                    return Math.pow((int) ((char) (op1)), (double) op2);
                } else if (op1 instanceof Boolean && op2 instanceof Double) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return Math.pow(o1, (double) op2);
                } else if (op1 instanceof Double && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return Math.pow((double) op1, o2);
                } else if (op1 instanceof Double && op2 instanceof Double) {
                    return Math.pow((double) op1, (double) op2);
                } //Tipo resultante de datos: Entero
                else if (op1 instanceof Integer && op2 instanceof Character) {
                    return Math.pow((int) op1, (int) ((char) op2));
                } else if (op1 instanceof Character && op2 instanceof Integer) {
                    return Math.pow((int) ((char) op1), (int) op2);
                } else if (op1 instanceof Boolean && op2 instanceof Integer) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return Math.pow(o1, (int) op2);
                } else if (op1 instanceof Integer && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return Math.pow((int) op1, o2);
                } else if (op1 instanceof Integer && op2 instanceof Integer) {
                    return Math.pow((int) op1, (int) op2);
                } //Tipo resultante de datos: Cadena
                else if (op1 instanceof String || op2 instanceof String) {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo el operador" + " potencia para dos cadenas");
                    return null;
                } //Tipo resultante de datos: Bool
                else if (op1 instanceof Boolean && op2 instanceof Boolean) {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo el operador" + " potencia para dos boolos");
                    return null;
                }
                break;
            case NEGATIVO:
                if (op1 instanceof Double) {
                    return 0.0 - (double) opU;
                } else if (opU instanceof Integer) {
                    return 0.0 - (int) opU;
                } else if (opU instanceof Character) {
                    return 0 - (int) ((char) opU);
                } else {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo el operador" + " menos unario incorrectamente");
                    return null;
                }
            case MAYOR_QUE:
                //Tipo resultante de datos: Decimal
                if (op1 instanceof Integer && op2 instanceof Double) {
                    return (int) op1 > (double) op2;
                } else if (op1 instanceof Double && op2 instanceof Integer) {
                    return (double) op1 > (int) op2;
                } else if (op1 instanceof Double && op2 instanceof Character) {
                    return (double) op2 > (int) ((char) (op2));
                } else if (op1 instanceof Character && op2 instanceof Double) {
                    return (int) ((char) (op1)) > (double) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Double) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return o1 > (double) op2;
                } else if (op1 instanceof Double && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return (double) op1 > o2;
                } else if (op1 instanceof Double && op2 instanceof Double) {
                    return (double) op1 > (double) op2;
                } //Tipo resultante de datos: Entero
                else if (op1 instanceof Integer && op2 instanceof Character) {
                    return (int) op1 > (int) ((char) op2);
                } else if (op1 instanceof Character && op2 instanceof Integer) {
                    return (int) ((char) op1) > (int) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Integer) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return o1 > (int) op2;
                } else if (op1 instanceof Integer && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return (int) op1 > o2;
                } else if (op1 instanceof Integer && op2 instanceof Integer) {
                    return (int) op1 > (int) op2;
                } //Tipo resultante de datos: Cadena
                else if (op1 instanceof String || op2 instanceof String) {
                    return op1.toString().length() > op2.toString().length();
                } //Tipo resultante de datos: Bool
                else if (op1 instanceof Boolean && op2 instanceof Boolean) {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo el operador" + " mayor que para dos boolos");
                    return null;
                }

            case MAYOR_IGUAL_QUE:
                //Tipo resultante de datos: Decimal
                if (op1 instanceof Integer && op2 instanceof Double) {
                    return (int) op1 >= (double) op2;
                } else if (op1 instanceof Double && op2 instanceof Integer) {
                    return (double) op1 >= (int) op2;
                } else if (op1 instanceof Double && op2 instanceof Character) {
                    return (double) op2 >= (int) ((char) (op2));
                } else if (op1 instanceof Character && op2 instanceof Double) {
                    return (int) ((char) (op1)) >= (double) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Double) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return o1 >= (double) op2;
                } else if (op1 instanceof Double && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return (double) op1 >= o2;
                } else if (op1 instanceof Double && op2 instanceof Double) {
                    return (double) op1 >= (double) op2;
                } //Tipo resultante de datos: Entero
                else if (op1 instanceof Integer && op2 instanceof Character) {
                    return (int) op1 >= (int) ((char) op2);
                } else if (op1 instanceof Character && op2 instanceof Integer) {
                    return (int) ((char) op1) >= (int) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Integer) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return o1 >= (int) op2;
                } else if (op1 instanceof Integer && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return (int) op1 >= o2;
                } else if (op1 instanceof Integer && op2 instanceof Integer) {
                    return (int) op1 >= (int) op2;
                } //Tipo resultante de datos: Cadena
                else if (op1 instanceof String || op2 instanceof String) {
                    return op1.toString().length() >= op2.toString().length();
                } //Tipo resultante de datos: Bool
                else if (op1 instanceof Boolean && op2 instanceof Boolean) {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo el operador" + " mayor que para dos boolos");
                    return null;
                }
                break;

            case MENOR_QUE:
                //Tipo resultante de datos: Decimal
                if (op1 instanceof Integer && op2 instanceof Double) {
                    return (int) op1 < (double) op2;
                } else if (op1 instanceof Double && op2 instanceof Integer) {
                    return (double) op1 < (int) op2;
                } else if (op1 instanceof Double && op2 instanceof Character) {
                    return (double) op2 < (int) ((char) (op2));
                } else if (op1 instanceof Character && op2 instanceof Double) {
                    return (int) ((char) (op1)) < (double) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Double) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return o1 < (double) op2;
                } else if (op1 instanceof Double && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return (double) op1 < o2;
                } else if (op1 instanceof Double && op2 instanceof Double) {
                    return (double) op1 < (double) op2;
                } //Tipo resultante de datos: Entero
                else if (op1 instanceof Integer && op2 instanceof Character) {
                    return (int) op1 < (int) ((char) op2);
                } else if (op1 instanceof Character && op2 instanceof Integer) {
                    return (int) ((char) op1) < (int) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Integer) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return o1 < (int) op2;
                } else if (op1 instanceof Integer && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return (int) op1 < o2;
                } else if (op1 instanceof Integer && op2 instanceof Integer) {
                    return (int) op1 < (int) op2;
                } //Tipo resultante de datos: Cadena
                else if (op1 instanceof String || op2 instanceof String) {
                    return op1.toString().length() < op2.toString().length();
                } //Tipo resultante de datos: Bool
                else if (op1 instanceof Boolean && op2 instanceof Boolean) {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo el operador" + " menor que para dos boolos");
                    return null;
                }
                break;
            case MENOR_IGUAL_QUE:
                //Tipo resultante de datos: Decimal
                if (op1 instanceof Integer && op2 instanceof Double) {
                    return (int) op1 <= (double) op2;
                } else if (op1 instanceof Double && op2 instanceof Integer) {
                    return (double) op1 <= (int) op2;
                } else if (op1 instanceof Double && op2 instanceof Character) {
                    return (double) op2 <= (int) ((char) (op2));
                } else if (op1 instanceof Character && op2 instanceof Double) {
                    return (int) ((char) (op1)) <= (double) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Double) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return o1 <= (double) op2;
                } else if (op1 instanceof Double && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return (double) op1 <= o2;
                } else if (op1 instanceof Double && op2 instanceof Double) {
                    return (double) op1 <= (double) op2;
                } //Tipo resultante de datos: Entero
                else if (op1 instanceof Integer && op2 instanceof Character) {
                    return (int) op1 <= (int) ((char) op2);
                } else if (op1 instanceof Character && op2 instanceof Integer) {
                    return (int) ((char) op1) <= (int) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Integer) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return o1 <= (int) op2;
                } else if (op1 instanceof Integer && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return (int) op1 <= o2;
                } else if (op1 instanceof Integer && op2 instanceof Integer) {
                    return (int) op1 <= (int) op2;
                } //Tipo resultante de datos: Cadena
                else if (op1 instanceof String || op2 instanceof String) {
                    return op1.toString().length() <= op2.toString().length();
                } //Tipo resultante de datos: Bool
                else if (op1 instanceof Boolean && op2 instanceof Boolean) {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo el operador" + " menor que para dos boolos");
                    return null;
                }
                break;
            case IGUAL_QUE:
                //Tipo resultante de datos: Decimal
                if (op1 instanceof Integer && op2 instanceof Double) {
                    return (int) op1 == (double) op2;
                } else if (op1 instanceof Double && op2 instanceof Integer) {
                    return (double) op1 == (int) op2;
                } else if (op1 instanceof Double && op2 instanceof Character) {
                    return (double) op2 == (int) ((char) (op2));
                } else if (op1 instanceof Character && op2 instanceof Double) {
                    return (int) ((char) (op1)) == (double) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Double) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return o1 == (double) op2;
                } else if (op1 instanceof Double && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return (double) op1 == o2;
                } else if (op1 instanceof Double && op2 instanceof Double) {
                    return (double) op1 == (double) op2;
                } //Tipo resultante de datos: Entero
                else if (op1 instanceof Integer && op2 instanceof Character) {
                    return (int) op1 == (int) ((char) op2);
                } else if (op1 instanceof Character && op2 instanceof Integer) {
                    return (int) ((char) op1) == (int) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Integer) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return o1 == (int) op2;
                } else if (op1 instanceof Integer && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return (int) op1 == o2;
                } else if (op1 instanceof Integer && op2 instanceof Integer) {
                    return (int) op1 == (int) op2;
                } //Tipo resultante de datos: Cadena
                else if (op1 instanceof String || op2 instanceof String) {
                    return op1.toString().equals(op2.toString());
                } //Tipo resultante de datos: Bool
                else if (op1 instanceof Boolean && op2 instanceof Boolean) {
                    return (boolean) op1 == (boolean) op2;
                }
                break;
            case DIFERENTE_QUE:
                //Tipo resultante de datos: Decimal
                if (op1 instanceof Integer && op2 instanceof Double) {
                    return (int) op1 != (double) op2;
                } else if (op1 instanceof Double && op2 instanceof Integer) {
                    return (double) op1 != (int) op2;
                } else if (op1 instanceof Double && op2 instanceof Character) {
                    return (double) op2 != (int) ((char) (op2));
                } else if (op1 instanceof Character && op2 instanceof Double) {
                    return (int) ((char) (op1)) != (double) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Double) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return o1 == (double) op2;
                } else if (op1 instanceof Double && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return (double) op1 != o2;
                } else if (op1 instanceof Double && op2 instanceof Double) {
                    return (double) op1 != (double) op2;
                } //Tipo resultante de datos: Entero
                else if (op1 instanceof Integer && op2 instanceof Character) {
                    return (int) op1 != (int) ((char) op2);
                } else if (op1 instanceof Character && op2 instanceof Integer) {
                    return (int) ((char) op1) != (int) op2;
                } else if (op1 instanceof Boolean && op2 instanceof Integer) {
                    int o1 = (boolean) op1 ? 1 : 0;
                    return o1 != (int) op2;
                } else if (op1 instanceof Integer && op2 instanceof Boolean) {
                    int o2 = (boolean) op1 ? 1 : 0;
                    return (int) op1 != o2;
                } else if (op1 instanceof Integer && op2 instanceof Integer) {
                    return (int) op1 != (int) op2;
                } //Tipo resultante de datos: Cadena
                else if (op1 instanceof String || op2 instanceof String) {
                    return !op1.toString().equals(op2.toString());
                } //Tipo resultante de datos: Bool
                else if (op1 instanceof Boolean && op2 instanceof Boolean) {
                    return (boolean) op1 != (boolean) op2;
                }
                break;

            case OR:
                if (op1 instanceof Boolean && op2 instanceof Boolean) {
                    return (boolean) op1 || (boolean) op2;
                } else {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo un operador" + " or, ambos operandos deben ser de tipo boolo");
                    return null;
                }
            case AND:
                if (op1 instanceof Boolean && op2 instanceof Boolean) {
                    return (boolean) op1 && (boolean) op2;
                } else {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo un operador" + " and, ambos operandos deben ser de tipo boolo");
                    return null;
                }
            case NOT:
                if (opU instanceof Boolean) {
                    return !(boolean) op1;
                } else {
                    SingletonArbol.getInstance().AstErrores.add("Error de tipos, se utilizo un operador" + " not, el operando debe ser de tipo boolo");
                    return null;
                }
            case NUMERO:
                return new Double(valor.toString());
            case IDENTIFICADOR:
                return ts.getValor(valor.toString());
            case CADENA:
                return valor.toString();
            case TRUE:
                return new Boolean(true);
            case FALSE:
                return new Boolean(false);
            default:
                return null;

        }

        return null;
    }
}
